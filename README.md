# Git scripts and hooks

Useful Git scripts and hooks.

## hooks/flat-site-post-commit

This hook can be used to install a flat site located in the `site` directory of the Git repo, copying it to `$HTDOCS`.

## hooks/jekyll-post-commit

This hook can be used to build and deploy a <a href="https://jekyllrb.com/">Jekyll</a> site into `$HTDOCS`.

### Git Logo license

Git Logo by Jason Long is licensed under the [Creative Commons Attribution 3.0 Unported License](https://creativecommons.org/licenses/by/3.0/).